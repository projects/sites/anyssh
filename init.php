<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : init.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : Init scripts
// last modified : 30.01.2012
//
// ###############################################################################################

/**
 * "PUBLIC" ROOT PATH
 */
define ( 'ETS_ROOT_PATH', str_replace ( "\\", "/", dirname ( __FILE__ ) ) . '/' );
require_once (ETS_ROOT_PATH . 'etersoft/anyssh/kernel/ETSKernel.php');
require_once (ETS_ROOT_PATH . 'etersoft/anyssh/db/ETSDB.php');
require_once (ETS_ROOT_PATH . 'etersoft/anyssh/db/ETSDBMySql.php');
require_once (ETS_ROOT_PATH . 'etersoft/anyssh/ETSActionListiner.php');

require_once (ETS_ROOT_PATH . 'etersoft/addons/class.phpmailer.php');
require_once (ETS_ROOT_PATH . 'etersoft/addons/class.pop3.php');
require_once (ETS_ROOT_PATH . 'etersoft/addons/class.smtp.php');
?>