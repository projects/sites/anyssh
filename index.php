<?php
#################################################################################################
#
#  project                   : Etersoft - AnySSH WebSite
#  filename                  : index.php
#  version                   : 1.0
#  last modified by          : Kuzmik Maxim
#  e-mail                    : forn@etersoft.ru
#  purpose                   : Main file for end-user ui.
#  last modified             : 30.01.2012
#
#################################################################################################

session_start ();
use etersoft\anyssh\kernel\ETSKernel;
use etersoft\anyssh\ETSActionListiner;

if(isset($_GET['lang'])) {
	$_SESSION['lang'] = $_GET['lang'];
}
if(!isset($_SESSION['lang'])) {
	$_SESSION['lang'] = 'en'; // default value
}

require_once 'init.php';
include 'etersoft/anyssh/localization/'.$_SESSION['lang'] . '.php';

$CURRENTUSER = null;
$kernel = ETSKernel::instance ();
$kernel->init ();
$actionListiner = ETSActionListiner::instance ();

$CURRENTUSER = null;
$result = $actionListiner->isUserLogged ();
if (isset ( $_REQUEST ['etsaction'] )) {
	echo $actionListiner->processAction ();
} else if (strcmp ( $result, "0" ) != 0) {
	$CURRENTUSER = $kernel->getUserById ( $result );
	require 'includes/work.php';
} else {
	require 'includes/login.php';
}
$kernel->getDB ()->disconnect ();
?>