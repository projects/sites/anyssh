<?php
#################################################################################################
#
#  project                   : Etersoft - AnySSH WebSite
#  filename                  : verify.php
#  version                   : 1.0
#  last modified by          : Kuzmik Maxim
#  e-mail                    : forn@etersoft.ru
#  purpose                   : Verify email by key.
#  last modified             : 30.01.2012
#
#################################################################################################
?>

<?php
session_start ();
require_once 'init.php';

use etersoft\anyssh\kernel\ETSKernel;
use etersoft\anyssh\ETSActionListiner;

$CURRENTUSER = null;
$kernel = ETSKernel::instance ();
$kernel->init ();
$actionListiner = ETSActionListiner::instance ();

$actionListiner->verifyUserEmail();

?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Etersoft - AnySSH - E-mail</title>
<link rel="stylesheet" type="text/css" href="anyssh.css">
</head>

<style type="text/css">
html,body {
	height: 100%;
}
</style>

<body style="background: #cfddea repeat-x;">
	<table width="100%" height="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="background: url(images/logo.png) center top no-repeat"
				align="center">
				<table class="panel" cellpadding="0" cellspacing="0"
					style="width: 300px">
					<tr>
						<td class="tl"></td>
						<td class="tm"></td>
						<td class="tr"></td>
					</tr>
					<tr>
						<td class="lm"></td>
						<td class="c">
							<table class="ftable">
								<tr>
									<td colspan="2" align="center"><div id="message"></div></td>
								</tr>
							</table>
							<form action="includes/action.php" method="post"
								id="formRegistration">
								<table class="ftable">
									<tr>
										<td colspan="2" align="center"><b>Your account now active: <a href="index.php">login</a></b></td>
									</tr>
								</table>
							</form>
						</td>
						<td class="rm"></td>
					</tr>
					<tr>
						<td class="bl"></td>
						<td class="bm"></td>
						<td class="br"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>

<?php 
$kernel->getDB ()->disconnect ();
?>