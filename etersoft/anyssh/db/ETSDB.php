<?php
namespace etersoft\anyssh\db;

/** 
 * Main interface for DB module
 * @author 		Maxim Kuzmik
 * @copyright	(c) 2012 - Etersoft.
 * @license		GPL
 * @package		etersoft.anyssh.db
 * @link		http://www.etersoft.com
 * @since		1.0
 * @version		1.0 
 */
interface ETSDB {
  
  /**
   * Connect to database server
   *
   * @return	boolean		Connection successful
   */
  public function connect();
  
  /**
   * Close database connection
   *
   * @return	boolean		Closed successfully
   */
  public function disconnect();
  
  /**
   * 
   * Execute select query
   * @param string $query
   */
  public function executeSelectQuery($query);
  
  /**
   * 
   * Execute insert, update, delete query (with out any result)
   * @param unknown_type $query
   */
  public function executeQuery($query);
  
}

?>