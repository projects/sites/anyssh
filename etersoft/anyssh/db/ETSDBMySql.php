<?php
namespace etersoft\anyssh\db;

use etersoft\anyssh\kernel\ETSKernel;

/**
 * MySQL implementation for DB.
 * @author 		Maxim Kuzmik
 * @copyright	(c) 2012 - Etersoft.
 * @license		GPL
 * @package		etersoft.anyssh.db
 * @link		http://www.etersoft.com
 * @since		1.0
 * @version		1.0
 */
class ETSDBMySql implements ETSDB {
  private static $connection = -1;
  
  /* (non-PHPdoc)
   * @see GEWDB::connect()
   */
  public function connect() {
    if (self::$connection==-1){
      self::$connection = mysql_connect(ETSKernel::$settings['sql_host'], ETSKernel::$settings['sql_user'], ETSKernel::$settings['sql_pass']) or die("Error connecting to the DB: ".mysql_errno());
      mysql_query("SET character_set_client='utf8'");
	  mysql_query("SET character_set_results='utf8'");
	  mysql_query("SET character_set_connection='utf8'"); 
      mysql_select_db(ETSKernel::$settings['sql_database'], self::$connection);
    }
  }

  /* (non-PHPdoc)
   * @see ETSDB::disconnect()
   */
  public function disconnect() {
    if (self::$connection!=-1){
      mysql_close(self::$connection);
    }
  }
  
  /**
   * (non-PHPdoc)
   * @see etersoft\anyssh\db.ETSDB::executeSelectQuery()
   */
  public function executeSelectQuery($query){
    $result = mysql_query($query, self::$connection) or die ('Ошибка в запросе: '.$query."\n".mysql_error());
    return $result; 
  }
  
  /**
   * (non-PHPdoc)
   * @see etersoft\anyssh\db.ETSDB::executeQuery()
   */
  public function executeQuery($query){
    mysql_query($query, self::$connection) or die ('Ошибка в запросе: '.$query."\n".mysql_error());
    $isInsert = stripos($query, "insert into");
    if ($isInsert !== true){
      return mysql_insert_id(self::$connection);
    }
  }
  

}
?>