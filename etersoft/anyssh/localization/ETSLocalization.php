<?php
namespace etersoft\anyssh\localization;

/**
 * For lozalization.
 *
 * @author Maxim Kuzmik
 * @copyright (c) 2012 - Etersoft.
 * @license GPL
 * @package etersoft.anyssh.db
 * @link http://www.etersoft.com
 * @since 1.0
 * @version 1.0
 */
class ETSLocalization {
	
	public static $DEFAULT_LOCALE = "en-en";

	/**
	 * Holds instance of ETSLocalization (singleton implementation)
	 *
	 * @access private
	 * @var object
	 */
	private static $instance;
	
	/**
	 * Initialize singleton
	 *
	 * @access public
	 * @return object
	 */
	public static function instance() {
		if (! self::$instance) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	private function __construct() {
	}
	
	public static function getLocalization(){
		$rootPath = str_replace ( "\\", "/", dirname ( __FILE__ ) ) . '/';
		$lang = ETSLocalization::$DEFAULT_LOCALE;
		
		if(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])){
			$lang = strtolower(substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, strpos($_SERVER["HTTP_ACCEPT_LANGUAGE"], ",")));
		}
		if(isset($_GET['lang'])) {
			$lang = $_GET['lang'];
		}
		if(isset($_REQUEST['lang'])) {
			$lang = $_REQUEST['lang'];
		}
		
		$filePath = $rootPath.'locales/'.$lang.".ini";
		if (!file_exists($filePath)){
			$lang = ETSLocalization::$DEFAULT_LOCALE;
		}
		return $lang; 
	}
	
	/**
	 * Return localization string by code.
	 * @param unknown_type $messageCode
	 */
	public static function getMessage($messageCode){
		$rootPath = str_replace ( "\\", "/", dirname ( __FILE__ ) ) . '/';
		
		$lang = ETSLocalization::getLocalization();
		$filePath = $rootPath.'locales/'.$lang.".ini";
		
		if (isset($messageCode)){
			if (file_exists($filePath)){
				$language = parse_ini_file($filePath, false);
				return $language[$messageCode]; 
			} else {
				return "No languages file:".$filePath;
			}
		} else {
			return '#Code is empty#';
		}
	}
	
}

?>