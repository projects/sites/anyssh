<?php
/**
 * Localization - Russian.
 * @author 		Maxim Kuzmik
 * @copyright	(c) 2012 - Etersoft.
 * @license		GPL
 * @package		etersoft.anyssh.db
 * @link		http://www.etersoft.com
 * @since		1.0
 * @version		1.0
 */

$localization = array(
		'Site.Login.Login'=>'Логин',
		'Site.Login.Password'=>'Пароль',
		'Site.Login.ProccesLogin'=>'Войти',
		'Site.Login.Registration'=>'Регистрация',
		
		'Site.Logout'=>'Выйти',
		
		'Site.Files.List'=>'Список ваших файлов',
		
		
		'Essence.Files.DateofCreate'=>'Дата создания',
		'Essence.Files.ExpiredDate'=>'Действителен до',
		
		'Essence.User.Login'=>'Логин',
		'Essence.User.Password'=>'Пароль',
		'Essence.User.ConfirmPassword'=>'Подтвердите пароль',
		'Essence.User.Email'=>'E-mail',
		'Essence.User.FullName'=>'Полное имя',
		'Essence.User.CompanyName'=>'Название компании',
		'Essence.User.SSHKey'=>'SSH ключ',
		
		'Messages.Register.Success'=>'Вы успешно зарегистрированы. Проверьте вашу почту.',
		'Messages.Register.AlreadyExist.Login'=>'Пользователь с введенном логином уже существует.',
		'Messages.Register.AlreadyExist.Email'=>'Пользователь с введенном E-mail адресом уже существует.',
		'Messages.Register.Mandatory.Login'=>'Поле "Логин" обязательно для заполнения.',
		'Messages.Register.Mandatory.Email'=>'Поле "E-mail" обязательно для заполнения.',
		'Messages.Register.Mandatory.SSH Key'=>'Поле "SSH ключ" обязательно для заполнения.',
		'Messages.Register.PwdNoitEquals'=>'Введенные пароли не совпадают.',
		'Messages.Register.Email.Subject'=>'Регистрация на сайте EterSoft AnySSH Site.',
		'Messages.Register.Email.Body'=>'Приветствуем. Для окончания регистрации вам необходимо пройти по ссылки:',
		
		'Messages.Login.InvalidLogin'=>'Пользователя не существует:',
		'Messages.Login.InvalidPassword'=>'Введенный пароль не правильный.',
		'Messages.Login.Blocked'=>'Ваша учетная запись блокирована.',
		'Messages.NoPermition.ViewPage'=>'У вас нет прав для просмотра данной странице.',
		
		'Action.Download'=>'Скачать',
		'Action.UpdateDownloadLink'=>'Обновить ссылку',
		'Action.Refresh'=>'Обновить',
		'Action.Register'=>'Зарегистрироваться',
		'Action.CreateFile'=>'Создать новый файл',
		
		'Site.Title'=>'Etersoft - AnySSH',
		'Site.Title.Login'=>'Вход в систему',
		'Site.Title.Registration'=>'Регистрация',
		'Site.Version'=>'Версия:'
				);
?>