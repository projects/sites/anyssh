<?php
/**
 * Localization - English.
 * @author 		Maxim Kuzmik
 * @copyright	(c) 2012 - Etersoft.
 * @license		GPL
 * @package		etersoft.anyssh.db
 * @link		http://www.etersoft.com
 * @since		1.0
 * @version		1.0
 */

$localization = array(
		'Site.Login.Login'=>'Login',
		'Site.Login.Password'=>'Password',
		'Site.Login.ProccesLogin'=>'Login',
		'Site.Login.Registration'=>'Registration',
		
		'Site.Logout'=>'Logout',
		
		'Site.Files.List'=>'List of your files',
		
		
		'Essence.Files.DateofCreate'=>'Date of creation',
		'Essence.Files.ExpiredDate'=>'Expired date',
		
		'Essence.User.Login'=>'Login',
		'Essence.User.Password'=>'Password',
		'Essence.User.ConfirmPassword'=>'Confirm Password',
		'Essence.User.Email'=>'Email',
		'Essence.User.FullName'=>'FullName',
		'Essence.User.CompanyName'=>'Company Name',
		'Essence.User.SSHKey'=>'SSH Key',
		
		'Messages.Register.Success'=>'You are successfully registered. Check your e-mail for details.',
		'Messages.Register.AlreadyExist.Login'=>'User with the same login already exist.',
		'Messages.Register.AlreadyExist.Email'=>'User with the same E-mail already exist.',
		'Messages.Register.Mandatory.Login'=>'Field "Login" must be not empty.',
		'Messages.Register.Mandatory.Email'=>'Field "E-mail" must be not empty.',
		'Messages.Register.Mandatory.SSH Key'=>'Field "SSH Key" must be not empty.',
		'Messages.Register.PwdNoitEquals'=>'Passwords are not equals.',
		'Messages.Register.Email.Subject'=>'Registration on the EterSoft AnySSH Site.',
		'Messages.Register.Email.Body'=>'Hell. You must confirm you email by visiting this URL: ',
		
		'Messages.Login.InvalidLogin'=>'There is not user with login:',
		'Messages.Login.InvalidPassword'=>'You are entered incorect password.',
		'Messages.Login.Blocked'=>'Your login is blocked.',
		'Messages.NoPermition.ViewPage'=>'You do not have permition to view this page.',
		
		'Action.Download'=>'Download',
		'Action.UpdateDownloadLink'=>'Update download link',
		'Action.Refresh'=>'Refresh',
		'Action.Register'=>'Register',
		'Action.CreateFile'=>'Create new file',
		
		'Site.Title'=>'Etersoft - AnySSH',
		'Site.Title.Login'=>'Login',
		'Site.Title.Registration'=>'Registration',
		'Site.Version'=>'Site Version'
				);
?>
