<?php
namespace etersoft\anyssh;

use etersoft\anyssh\kernel\ETSKernel;
use etersoft\addons\PHPMailer;

/**
 * Proccess action from users.
 *
 * @author Maxim Kuzmik
 * @copyright (c) 2012 - Etersoft.
 * @license GPL
 * @package etersoft.anyssh.db
 * @link http://www.etersoft.com
 * @since 1.0
 * @version 1.0
 */
class ETSActionListiner {
	
	/**
	 * Holds instance of ETSActionListiner (singleton implementation)
	 *
	 * @access private
	 * @var object
	 */
	private static $instance;
	
	/**
	 * Initialize singleton
	 *
	 * @access public
	 * @return object
	 */
	public static function instance() {
		if (! self::$instance) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	private function __construct() {
	}
	
	/**
	 * Return curent user id.
	 * Else return 0.
	 */
	public function isUserLogged() {
		if (isset ( $_SESSION ['userId'] )) {
			return $_SESSION ['userId'];
		} else {
			if (isset ( $_COOKIE ['etsuser_id'] )) {
				return $_COOKIE ['etsuser_id'];
			} else {
				return "0";
			}
		}
		return "0";
	}
	
	/**
	 * Proccess Action.
	 *
	 * @param
	 *       	 $isAjax
	 */
	public function processAction($isAjax) {
		$action = $_REQUEST ['etsaction'];
		if (strcmp ( $action, "userlogin" ) == 0) {
			return $this->action_userlogin ();
		} else if (strcmp ( $action, "userlogout" ) == 0) {
			return $this->action_userlogout ();
		} else if (strcmp ( $action, "refreshDownloadKey" ) == 0) {
			return $this->action_refreshDownloadKey ();
		} else if (strcmp ( $action, "userRegistration" ) == 0) {
			return $this->action_userRegistration ();
		} else if (strcmp ( $action, "createNewFile" ) == 0) {
			return $this->action_createNewFile ();
		} else {
			return "Unknow action code:" + $action;
		}
	}
	
	private function action_createNewFile(){
		$kernel = ETSKernel::instance ();
		
		// Create file
		$fileData = self::generateFileForUser($_SESSION ['userId']);
		
		$newDate = new \DateTime ( "now", new \DateTimeZone ( 'Europe/Moscow' ) );
		$query = "INSERT INTO ANYSSH_FILES (A_USERS_OUID, A_CREATED_DATE, A_FILEPATH, A_SYSTEM_USER, A_EXPIRED_DATE, A_DOWNLOADKEY) VALUES (" . $_SESSION ['userId'] . ",STR_TO_DATE('" . $newDate->format ( 'd.m.Y H:i' ) . "', '%d.%m.%Y %H:%i'),'" . $fileData ["A_FILEPATH"] . "','" . $fileData ["A_SYSTEM_USER"] . "','" . $fileData ["A_EXPIRED_DATE"] . "','" . $fileData ["A_DOWNLOADKEY"] . "')";
		$kernel->getDB ()->executeQuery ( $query );
	}
	
	/**
	 * Generate file for user
	 * @param unknown_type $userId
	 */
	public function generateFileForUSer($userId) {
		shell_exec('sh /home/anyssh/www/anyssh.ru/scripts/anyssh/create_copy_sandbox.sh');
		$downloadKey = uniqid ( "etsfiles_" );
		$myPATH = trim(file_get_contents('/home/anyssh/www/anyssh.ru/scripts/anyssh/path'));
		$fileData = array ('A_FILEPATH' => $myPATH, 'A_SYSTEM_USER' => 'systemUser', 'A_EXPIRED_DATE' => '2012-03-01 00:00', 'A_DOWNLOADKEY' => $downloadKey );
		
		return $fileData;
	}
	
	/**
	 * Register the new user.
	 */
	private function action_userRegistration() {
		include '../etersoft/anyssh/localization/'.$_SESSION['lang'] . '.php';
		
		$isExist = self::checkUserByLogin ( $_POST ['user'] );
		if ($isExist == 1) {
			return $localization['Messages.Register.AlreadyExist.Login'];
		}
		$isExist = self::checkUserByEmail ( $_POST ['email'] );
		if ($isExist == 1) {
			return $localization['Messages.Register.AlreadyExist.Email'];
		}
		
		if (strcmp ( $_POST ['user'], "" ) == 0) {
			return $localization['Messages.Register.Mandatory.Login'];
		}
		if (strcmp ( $_POST ['password'], $_POST ['password2'] ) != 0) {
			return $localization['Messages.Register.PwdNoitEquals'];
		}
		if (strcmp ( $_POST ['email'], "" ) == 0) {
			return $localization['Messages.Register.Mandatory.Email'];
		}
		if (strcmp ( $_POST ['sshKey'], "" ) == 0) {
			return $localization['Messages.Register.Mandatory.SSHKey'];
		}
		
		$kernel = ETSKernel::instance ();
		
		$emailKey = uniqid ( "etsemail_" );
		
		$query = "INSERT INTO ANYSSH_USERS (A_LOGIN, A_PASSWORD, A_EMAIL, A_FIO, A_ORGANIZATION, A_ENABLED, A_SSHKEY, A_EMAIL_VALID) VALUES ('" . $_POST ['user'] . "','" . md5 ( $_POST ['password'] ) . "','" . $_POST ['email'] . "','" . $_POST ['fullName'] . "','" . $_POST ['companyName'] . "',0,'" . $_POST ["sshKey"] . "','" . $emailKey . "')";
		$kernel->getDB ()->executeQuery ( $query );
		
		// Send e-mail
		$mail = new PHPMailer ();
		$mail->IsSMTP ();
		$mail->Host = "mail.etersoft.ru";
		$mail->SMTPAuth = true;
		$mail->Port = 587;
		$mail->Username = "";
		$mail->Password = "";
		
		$mail->SetFrom ( 'support@etersoft.ru', 'Etersoft Support Team' );
		$mail->Subject = $localization['Messages.Register.Email.Subject'];
		
		$address = $_POST ['email'];
		$mail->AddAddress ( $address, $_POST ['user'] );
		
		$url = "http://";
		/*$sn = $_SERVER['SCRIPT_NAME'];
		$dotPos = strpos($sn,"/", 2);
		$snCut = substr($sn, 0, $dotPos+1);*/
		
		if ($_SERVER ['SERVER_PORT'] != 80) {
			$url .=  $_SERVER ['SERVER_NAME'] . ":".$_SERVER ['SERVER_PORT']/*.$snCut*/;
		} else {
			$url .=   $_SERVER ['SERVER_NAME']/*.$snCut*/;
		}
		$mail->Body = $localization['Messages.Register.Email.Body']." ".$url."/verify.php" . "?key=" . $emailKey;
		if (! $mail->Send ()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "0";
		}
	}
	
	/**
	 * Active account by email key.
	 */
	public function verifyUserEmail() {
		if (isset ( $_GET ['key'] )) {
			$kernel = ETSKernel::instance ();
			$key = $_GET ['key'];
			
			$query = "update ANYSSH_USERS set A_ENABLED=1 where A_EMAIL_VALID='" . $key . "'";
			$kernel->getDB ()->executeQuery ( $query );
		}
	}
	
	/**
	 * Update file.
	 */
	private function action_refreshDownloadKey() {
		$kernel = ETSKernel::instance ();
		
		$fileId = $_POST ['fileId'];
		$newDate = new \DateTime ( "now", new \DateTimeZone ( 'Europe/Moscow' ) );
		$newDate->add ( new \DateInterval ( 'P10D' ) );
		$downloadKey = uniqid ( "etsfiles_" );
		
		$query = "update ANYSSH_FILES set A_EXPIRED_DATE=STR_TO_DATE('" . $newDate->format ( 'd.m.Y H:i' ) . "', '%d.%m.%Y %H:%i'), A_DOWNLOADKEY='" . $downloadKey . "' where A_OUID=" . $fileId;
		$kernel->getDB ()->executeQuery ( $query );
		
		return "0";
	
	}
	
	/**
	 * Login to the system.
	 */
	private function action_userlogin() {
		include '../etersoft/anyssh/localization/'.$_SESSION['lang'] . '.php';
		
		$kernel = ETSKernel::instance ();
		$user = $kernel->getUserByLogin ( $_POST ['user'] );
		if (! isset ( $user )) {
			return $localization['Messages.Login.InvalidLogin']."'" . $_POST ['user'] . "'.";
		} else {
			if (strcmp ( $user ['A_PASSWORD'], md5 ( $_POST ['password'] ) ) != 0) {
				return $localization['Messages.Login.InvalidPassword'];
			}
			if (strcmp ( $user ['A_ENABLED'], "1" ) != 0) {
				return $localization['Messages.Login.Blocked'];
			}
		}
		
		if (isset ( $_POST ['isadmin'] )) {
			// Проверка на права администратора
			if (! $kernel->isUserInRole ( $user ['A_OUID'], "admin" )) {
				return $localization['Messages.NoPermition.ViewPage'];
			}
		}
		
		$_SESSION ['userId'] = $user ['A_OUID'];
		return 0;
	}
	
	/**
	 * Logout from system.
	 */
	private function action_userlogout() {
		if (isset ( $_COOKIE ["etsuser_id"] )) {
			setcookie ( "etsuser_id", $_COOKIE ["etsuser_id"], time () - 100, "/" ); // специально
				                                                                         // для
				                                                                         // firefox
				                                                                         // (в
				                                                                         // версии
				                                                                         // 3.5.9
				                                                                         // обнаружил)
		}
		$_SESSION = array ();
		unset ( $_SESSION ['userId'] );
		unset ( $_COOKIE [session_name ()] );
		return 0;
	}
	
	/**
	 * Get all users order by login.
	 */
	public function getAllUsers() {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT * from ANYSSH_USERS order by A_LOGIN";
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		return $rs;
	}
	
	/**
	 * Get all roles for user.
	 */
	public function getUsersPermitions() {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT `anyssh_userroles`.`A_OUID`,  
						`anyssh_roles`.`A_NAME`,  
						`anyssh_users`.`A_LOGIN` 
				FROM   `anyssh_userroles`  
					INNER JOIN `anyssh_users` ON (`anyssh_userroles`.`A_USERS_OUID` = `anyssh_users`.`A_OUID`)   
					INNER JOIN `anyssh_roles` ON (`anyssh_userroles`.`A_ROLES_OUID` = `anyssh_roles`.`A_OUID`) 
				ORDER BY `anyssh_users`.`A_LOGIN`";
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		return $rs;
	}
	
	/**
	 * Ge user role by id.
	 *
	 * @param $essenceId unknown_type       	
	 */
	public function getUserPermitionById($essenceId) {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT `anyssh_userroles`.`A_OUID`,
		`anyssh_roles`.`A_NAME`,
		`anyssh_users`.`A_LOGIN`,
		`anyssh_roles`.`A_OUID` AS `roleouid`,
  		`anyssh_users`.`A_OUID` AS `userouid`
		FROM   `anyssh_userroles`
		INNER JOIN `anyssh_users` ON (`anyssh_userroles`.`A_USERS_OUID` = `anyssh_users`.`A_OUID`)
		INNER JOIN `anyssh_roles` ON (`anyssh_userroles`.`A_ROLES_OUID` = `anyssh_roles`.`A_OUID`)
		WHERE `anyssh_userroles`.`A_OUID`=" . $essenceId . "
		ORDER BY `anyssh_users`.`A_LOGIN`";
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		$row = mysql_fetch_assoc ( $rs );
		return $row;
	}
	
	/**
	 * Get all roles order by name.
	 */
	public function getAllRoles() {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT * from ANYSSH_ROLES order by A_NAME";
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		return $rs;
	}
	
	/**
	 * Get all files order by user name.
	 */
	public function getAllFiles() {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT 
			  `anyssh_users`.`A_LOGIN`,
			  `anyssh_files`.`A_OUID`,
			  `anyssh_files`.`A_USERS_OUID`,
			  `anyssh_files`.`A_CREATED_DATE`,
			  `anyssh_files`.`A_FILEPATH`,
			  `anyssh_files`.`A_SYSTEM_USER`,
			  `anyssh_files`.`A_EXPIRED_DATE`,
			  `anyssh_files`.`A_DOWNLOADKEY`
			FROM
			  `anyssh_files`
			  INNER JOIN `anyssh_users` ON (`anyssh_files`.`A_USERS_OUID` = `anyssh_users`.`A_OUID`)
			ORDER BY
			  `anyssh_users`.`A_LOGIN`";
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		return $rs;
	}
	
	/**
	 * Get file by Id.
	 *
	 * @param $essenceId unknown_type       	
	 */
	public function getAnySSHFileById($essenceId) {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT 
			  `anyssh_users`.`A_LOGIN`,
			  `anyssh_files`.`A_OUID`,
			  `anyssh_files`.`A_USERS_OUID`,
			  `anyssh_files`.`A_CREATED_DATE`,
			  `anyssh_files`.`A_FILEPATH`,
			  `anyssh_files`.`A_SYSTEM_USER`,
			  `anyssh_files`.`A_EXPIRED_DATE`,
			  `anyssh_files`.`A_DOWNLOADKEY`
			FROM
			  `anyssh_files`
			  INNER JOIN `anyssh_users` ON (`anyssh_files`.`A_USERS_OUID` = `anyssh_users`.`A_OUID`)
			WHERE `anyssh_files`.`A_OUID`=" . $essenceId . "
			ORDER BY
			  `anyssh_users`.`A_LOGIN`";
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		$row = mysql_fetch_assoc ( $rs );
		return $row;
	}
	
/**
	 * Get file by Id.
	 *
	 * @param $essenceId unknown_type       	
	 */
	public function getAnySSHFileByKey($key) {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT 
			  `anyssh_users`.`A_LOGIN`,
			  `anyssh_files`.`A_OUID`,
			  `anyssh_files`.`A_USERS_OUID`,
			  `anyssh_files`.`A_CREATED_DATE`,
			  `anyssh_files`.`A_FILEPATH`,
			  `anyssh_files`.`A_SYSTEM_USER`,
			  `anyssh_files`.`A_EXPIRED_DATE`,
			  `anyssh_files`.`A_DOWNLOADKEY`
			FROM
			  `anyssh_files`
			  INNER JOIN `anyssh_users` ON (`anyssh_files`.`A_USERS_OUID` = `anyssh_users`.`A_OUID`)
			WHERE `anyssh_files`.`A_DOWNLOADKEY`='" . $key . "'
			ORDER BY
			  `anyssh_users`.`A_LOGIN`";
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		$row = mysql_fetch_assoc ( $rs );
		return $row;
	}
	
	/**
	 * Get role by id.
	 */
	public function getRoleById($roleId) {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT * from ANYSSH_ROLES where A_OUID=" . $roleId;
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		$row = mysql_fetch_assoc ( $rs );
		return $row;
	}
	
	/**
	 * Get user by id.
	 */
	public function getUserById($userId) {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT * from ANYSSH_USERS where A_OUID=" . $userId;
		
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		$row = mysql_fetch_assoc ( $rs );
		return $row;
	}
	
	/**
	 * Is user exist.
	 */
	public function checkUserByLogin($userLogin) {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT * from ANYSSH_USERS where A_LOGIN='" . $userLogin . "'";
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		if (isset ( $rs )) {
			$row = mysql_fetch_assoc ( $rs );
			if ($row ["A_OUID"] > 0) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	/**
	 * Is user exist.
	 */
	public function checkUserByEmail($userEmail) {
		$kernel = ETSKernel::instance ();
		
		$query = "SELECT * from ANYSSH_USERS where A_EMAIL='" . $userEmail . "'";
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		if (isset ( $rs )) {
			$row = mysql_fetch_assoc ( $rs );
			if ($row ["A_OUID"] > 0) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	/**
	 * Generate xml data fro grid with roles.
	 */
	public function generateXmlGrid_Roles() {
		$resultXML = '<rows>';
		$resultXML .= '<head>';
		
		// Id
		$columnType = "rotxt";
		$width = "55";
		$sort = "int";
		$attrTitle = 'Identity';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Name
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Name';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Code
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Code';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		$resultXML .= '</head>';
		
		$userList = self::getAllRoles ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			$resultXML .= '<row id="' . $row ["A_OUID"] . '">';
			$resultXML .= '<cell>' . $row ["A_OUID"] . '</cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_NAME"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_CODE"] . ']]></cell>';
			$resultXML .= '</row>';
		}
		$resultXML .= '</rows>';
		
		return $resultXML;
	}
	
	/**
	 * Generate xml data fro grid with users.
	 */
	public function generateXmlGrid_Users() {
		$resultXML = '<rows>';
		$resultXML .= '<head>';
		
		// Id
		$columnType = "rotxt";
		$width = "55";
		$sort = "int";
		$attrTitle = 'Identity';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Login
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Login';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Name
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Name';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// E-mail
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'E-mail';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Organization
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Organization';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Enabled
		$columnType = "ch";
		$width = "55";
		$sort = "str";
		$attrTitle = 'Enabled';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		$resultXML .= '</head>';
		
		$userList = self::getAllUsers ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			$resultXML .= '<row id="' . $row ["A_OUID"] . '">';
			$resultXML .= '<cell>' . $row ["A_OUID"] . '</cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_LOGIN"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_FIO"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_EMAIL"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_ORGANIZATION"] . ']]></cell>';
			$resultXML .= '<cell>' . $row ["A_ENABLED"] . '</cell>';
			$resultXML .= '</row>';
		}
		$resultXML .= '</rows>';
		return $resultXML;
	}
	
	/**
	 * Generate xml data fro grid with users roles.
	 */
	public function generateXmlGrid_UsersPermitions() {
		$resultXML = '<rows>';
		$resultXML .= '<head>';
		
		// Id
		$columnType = "rotxt";
		$width = "55";
		$sort = "int";
		$attrTitle = 'Identity';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// User
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Login';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Role
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Name';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		$resultXML .= '</head>';
		
		$userList = self::getUsersPermitions ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			$resultXML .= '<row id="' . $row ["A_OUID"] . '">';
			$resultXML .= '<cell>' . $row ["A_OUID"] . '</cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_LOGIN"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_NAME"] . ']]></cell>';
			$resultXML .= '</row>';
		}
		$resultXML .= '</rows>';
		return $resultXML;
	}
	
	/**
	 * Generate xml data fro grid with files.
	 */
	public function generateXmlGrid_AnySSHFiles() {
		$resultXML = '<rows>';
		$resultXML .= '<head>';
		
		// Id
		$columnType = "rotxt";
		$width = "55";
		$sort = "int";
		$attrTitle = 'Identity';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Owner
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Owner';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Created date
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Created date';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// File path
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'File path';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// System user
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'System user';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		// Expired date
		$columnType = "rotxt";
		$width = "200";
		$sort = "str";
		$attrTitle = 'Expired date';
		$resultXML .= '<column width="' . $width . '" sort="' . $sort . '" type="' . $columnType . '"><![CDATA[' . $attrTitle . ']]></column>';
		
		$resultXML .= '</head>';
		
		$userList = self::getAllFiles ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			$resultXML .= '<row id="' . $row ["A_OUID"] . '">';
			$resultXML .= '<cell>' . $row ["A_OUID"] . '</cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_LOGIN"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_CREATED_DATE"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_FILEPATH"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_SYSTEM_USER"] . ']]></cell>';
			$resultXML .= '<cell><![CDATA[' . $row ["A_EXPIRED_DATE"] . ']]></cell>';
			$resultXML .= '</row>';
		}
		$resultXML .= '</rows>';
		
		return $resultXML;
	}
	
	/**
	 * Generate edit form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	public function generateXmlEditForm($folderId, $essenceId) {
		if (strcmp ( $folderId, "security_roles" ) == 0) {
			return self::generateXmlEditForm_Roles ( $folderId, $essenceId );
		} else if (strcmp ( $folderId, "security_users" ) == 0) {
			return self::generateXmlEditForm_Users ( $folderId, $essenceId );
		} else if (strcmp ( $folderId, "security_userroles" ) == 0) {
			return self::generateXmlEditForm_UsersPermitions ( $folderId, $essenceId );
		} else if (strcmp ( $folderId, "anyssh_files" ) == 0) {
			return self::generateXmlEditForm_AnySSHFiles ( $folderId, $essenceId );
		}
	}
	
	/**
	 * Generate edit form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	private function generateXmlEditForm_Roles($folderId, $essenceId) {
		$role = self::getRoleById ( $essenceId );
		
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="Identity: ' . $essenceId . '"/>';
		
		// Name
		$resultXML .= '<item type="label" label="Name:"/>';
		$resultXML .= '<item type="input" name="A_NAME" validate="NotEmpty" value="' . $role ["A_NAME"] . '" inputWidth="250"/>';
		
		$resultXML .= '<item type="label" label="Code:"/>';
		$resultXML .= '<item type="input" name="A_CODE" validate="NotEmpty" value="' . $role ["A_CODE"] . '" inputWidth="250"/>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="essenceId" value="' . $essenceId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="updateEssence"/>';
		
		$resultXML .= '<item type="button" value="Save changes" name="btnEditSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Generate create form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	public function generateXmlCreateForm_Roles($folderId) {
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="Name:"/>';
		$resultXML .= '<item type="input" name="A_NAME" validate="NotEmpty" inputWidth="250"/>';
		
		$resultXML .= '<item type="label" label="Code:"/>';
		$resultXML .= '<item type="input" name="A_CODE" validate="NotEmpty" inputWidth="250"/>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="createEssence"/>';
		
		$resultXML .= '<item type="button" value="Create" name="btnAddSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Generate edit form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	private function generateXmlEditForm_UsersPermitions($folderId, $essenceId) {
		$permition = self::getUserPermitionById ( $essenceId );
		
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="Identity: ' . $essenceId . '"/>';
		
		$resultXML .= '<item type="label" label="User:"/>';
		$resultXML .= '<item type="select" name="user" validate="NotEmpty" inputWidth="300">';
		$userList = self::getAllUsers ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			if ($row ["A_OUID"] == $permition ["userouid"]) {
				$resultXML .= '<option text="' . $row ["A_LOGIN"] . '" value="' . $row ["A_OUID"] . '" selected="true"/>';
			} else {
				$resultXML .= '<option text="' . $row ["A_LOGIN"] . '" value="' . $row ["A_OUID"] . '" />';
			}
		}
		$resultXML .= '</item>';
		
		$resultXML .= '<item type="label" label="Role:"/>';
		$resultXML .= '<item type="select" name="role" validate="NotEmpty" inputWidth="300">';
		$roleList = self::getAllRoles ();
		for($count = 1; $row = mysql_fetch_assoc ( $roleList ); ++ $count) {
			if ($row ["A_OUID"] == $permition ["userouid"]) {
				$resultXML .= '<option text="' . $row ["A_NAME"] . '" value="' . $row ["A_OUID"] . '" selected="true"/>';
			} else {
				$resultXML .= '<option text="' . $row ["A_NAME"] . '" value="' . $row ["A_OUID"] . '" />';
			}
		}
		$resultXML .= '</item>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="essenceId" value="' . $essenceId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="updateEssence"/>';
		
		$resultXML .= '<item type="button" value="Update" name="btnEditSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Generate edit form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	private function generateXmlEditForm_AnySSHFiles($folderId, $essenceId) {
		$anySHHFile = self::getAnySSHFileById ( $essenceId );
		
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="Identity: ' . $essenceId . '"/>';
		
		$resultXML .= '<item type="label" label="File owner:"/>';
		$resultXML .= '<item type="select" name="A_USERS_OUID" validate="NotEmpty" inputWidth="300">';
		$userList = self::getAllUsers ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			if ($row ["A_OUID"] == $anySHHFile ["A_USERS_OUID"]) {
				$resultXML .= '<option text="' . $row ["A_LOGIN"] . '" value="' . $row ["A_OUID"] . '" selected="true"/>';
			} else {
				$resultXML .= '<option text="' . $row ["A_LOGIN"] . '" value="' . $row ["A_OUID"] . '" />';
			}
		}
		$resultXML .= '</item>';
		
		$resultXML .= '<item type="label" label="Created date: ' . $anySHHFile ["A_CREATED_DATE"] . '"/>';
		
		$resultXML .= '<item type="label" label="File path:"/>';
		$resultXML .= '<item type="input" name="A_FILEPATH" validate="NotEmpty" value="' . $anySHHFile ["A_FILEPATH"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="System user:"/>';
		$resultXML .= '<item type="input" name="A_SYSTEM_USER" validate="NotEmpty" value="' . $anySHHFile ["A_SYSTEM_USER"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Expired date:"/>';
		$resultXML .= '<item type="input" name="A_EXPIRED_DATE" validate="NotEmpty" value="' . $anySHHFile ["A_EXPIRED_DATE"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Download key:"/>';
		$resultXML .= '<item type="input" name="A_DOWNLOADKEY" validate="NotEmpty" value="' . $anySHHFile ["A_DOWNLOADKEY"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="essenceId" value="' . $essenceId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="updateEssence"/>';
		
		$resultXML .= '<item type="button" value="Update" name="btnEditSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Generate create form.
	 *
	 * @param $folderId unknown_type       	
	 */
	public function generateXmlCreateForm_AnySSHFiles($folderId) {
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="File owner:"/>';
		$resultXML .= '<item type="select" name="A_USERS_OUID" validate="NotEmpty" inputWidth="300">';
		$userList = self::getAllUsers ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			$resultXML .= '<option text="' . $row ["A_LOGIN"] . '" value="' . $row ["A_OUID"] . '" />';
		}
		$resultXML .= '</item>';
		
		$resultXML .= '<item type="label" label="Created date (yyyy-mm-dd hh:mi):"/>';
		$resultXML .= '<item type="input" name="A_CREATED_DATE" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="File path:"/>';
		$resultXML .= '<item type="input" name="A_FILEPATH" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="System user:"/>';
		$resultXML .= '<item type="input" name="A_SYSTEM_USER" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Expired date:"/>';
		$resultXML .= '<item type="input" name="A_EXPIRED_DATE" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Download key:"/>';
		$resultXML .= '<item type="input" name="A_DOWNLOADKEY" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="createEssence"/>';
		
		$resultXML .= '<item type="button" value="Create" name="btnAddSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Generate edit form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	private function generateXmlEditForm_Users($folderId, $essenceId) {
		$user = self::getUserPermitionById ( $essenceId );
		
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="Identity: ' . $essenceId . '"/>';
		
		$resultXML .= '<item type="label" label="Login:"/>';
		$resultXML .= '<item type="input" name="A_LOGIN" validate="NotEmpty" value="' . htmlspecialchars ( $user ["A_LOGIN"] ) . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Role:"/>';
		$resultXML .= '<item type="password" name="A_PASSWORD" validate="NotEmpty" value="' . $user ["A_PASSWORD"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Password Confirm:"/>';
		$resultXML .= '<item type="password" name="A_PASSWORD2" validate="NotEmpty" value="' . $user ["A_PASSWORD"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="E-mail:"/>';
		$resultXML .= '<item type="input" name="A_EMAIL" validate="ValidEmail" value="' . $user ["A_EMAIL"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Name:"/>';
		$resultXML .= '<item type="input" name="A_FIO" validate="NotEmpty" value="' . $user ["A_FIO"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Organization:"/>';
		$resultXML .= '<item type="input" name="A_ORGANIZATION" value="' . htmlspecialchars ( $user ["A_ORGANIZATION"] ) . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Enabled:"/>';
		if ($user ["A_ENABLED"] == 0) {
			$resultXML .= '<item type="checkbox" name="A_ENABLED"/>';
		} else {
			$resultXML .= '<item type="checkbox" name="A_ENABLED" checked="true"/>';
		}
		
		$resultXML .= '<item type="label" label="E-mail Key:"/>';
		$resultXML .= '<item type="input" name="A_EMAIL_VALID" value="' . $user ["A_EMAIL_VALID"] . '" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="SSH-Key:"/>';
		$resultXML .= '<item type="input" name="A_SSHKEY" value="' . $user ["A_SSHKEY"] . '" inputWidth="300" rows="5"/>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="essenceId" value="' . $essenceId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="updateEssence"/>';
		
		$resultXML .= '<item type="button" value="Update" name="btnEditSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Generate create form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	public function generateXmlCreateForm_Users($folderId) {
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="Login:"/>';
		$resultXML .= '<item type="input" name="A_LOGIN" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Password:"/>';
		$resultXML .= '<item type="password" name="A_PASSWORD" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Password Confirm:"/>';
		$resultXML .= '<item type="password" name="A_PASSWORD2" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="E-mail:"/>';
		$resultXML .= '<item type="input" name="A_EMAIL" validate="ValidEmail" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Name:"/>';
		$resultXML .= '<item type="input" name="A_FIO" validate="NotEmpty" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Organization:"/>';
		$resultXML .= '<item type="input" name="A_ORGANIZATION" inputWidth="300"/>';
		
		$resultXML .= '<item type="label" label="Enabled:"/>';
		$resultXML .= '<item type="checkbox" name="A_ENABLED"/>';
		
		$resultXML .= '<item type="label" label="SSH-Key:"/>';
		$resultXML .= '<item type="input" name="A_SSHKEY" inputWidth="300" rows="5"/>';
		
		$resultXML .= '<item type="label" label="E-mail Key:"/>';
		$resultXML .= '<item type="input" name="A_EMAIL_VALID" inputWidth="300"/>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="createEssence"/>';
		
		$resultXML .= '<item type="button" value="Create" name="btnAddSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Generate create form.
	 *
	 * @param $folderId unknown_type       	
	 * @param $essenceId unknown_type       	
	 */
	public function generateXmlCreateForm_UsersPermitions($folderId) {
		$resultXML = "<items>";
		
		$resultXML .= '<item type="label" label="User:"/>';
		$resultXML .= '<item type="select" name="user" validate="NotEmpty" inputWidth="300">';
		$userList = self::getAllUsers ();
		for($count = 1; $row = mysql_fetch_assoc ( $userList ); ++ $count) {
			$resultXML .= '<option text="' . $row ["A_LOGIN"] . '" value="' . $row ["A_OUID"] . '" />';
		}
		$resultXML .= '</item>';
		
		$resultXML .= '<item type="label" label="Role:"/>';
		$resultXML .= '<item type="select" name="role" validate="NotEmpty" inputWidth="300">';
		$roleList = self::getAllRoles ();
		for($count = 1; $row = mysql_fetch_assoc ( $roleList ); ++ $count) {
			$resultXML .= '<option text="' . $row ["A_NAME"] . '" value="' . $row ["A_OUID"] . '" />';
		}
		$resultXML .= '</item>';
		
		$resultXML .= '<item type="hidden" name="folderid" value="' . $folderId . '"/>';
		$resultXML .= '<item type="hidden" name="etsaction" value="createEssence"/>';
		
		$resultXML .= '<item type="button" value="Create" name="btnAddSubmit"/>';
		
		$resultXML .= "</items>";
		return $resultXML;
	}
	
	/**
	 * Update object from Request.
	 */
	public function updateEssence($newData) {
		$folderId = $newData ['folderid'];
		$essenceId = $newData ['essenceId'];
		$kernel = ETSKernel::instance ();
		
		if (strcmp ( $folderId, "security_roles" ) == 0) {
			// Update role
			$query = "update ANYSSH_ROLES set A_NAME='" . $newData ["A_NAME"] . "', A_CODE='" . $newData ["A_CODE"] . "' where A_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
		} else if (strcmp ( $folderId, "security_users" ) == 0) {
			
			$password1 = $newData ["A_PASSWORD"];
			$password2 = $newData ["A_PASSWORD2"];
			if (strcmp ( $password1, $password2 ) != 0) {
				// Password not equals
				return "Password not equals.";
			} else {
				// Update users
				$query = "update ANYSSH_USERS set A_LOGIN='" . $newData ["A_LOGIN"] . "', A_PASSWORD='" . md5 ( $newData ["A_PASSWORD"] ) . "', A_EMAIL='" . $newData ["A_EMAIL"] . "', A_FIO='" . $newData ["A_FIO"] . "', A_ENABLED=" . $newData ["A_ENABLED"] . ", A_ORGANIZATION='" . $newData ["A_ORGANIZATION"] . "', A_SSHKEY='" . $newData ["A_SSHKEY"] . "', A_EMAIL_VALID='" . $newData ["A_EMAIL_VALID"] . "' where A_OUID=" . $essenceId;
				$kernel->getDB ()->executeQuery ( $query );
			}
		} else if (strcmp ( $folderId, "security_userroles" ) == 0) {
			// Update users
			$query = "update ANYSSH_USERROLES set A_USERS_OUID='" . $newData ["user"] . "', A_ROLES_OUID='" . $newData ["role"] . "' where A_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
		} else if (strcmp ( $folderId, "anyssh_files" ) == 0) {
			// Update files
			$query = "update ANYSSH_FILES set A_USERS_OUID='" . $newData ["A_USERS_OUID"] . "', A_FILEPATH='" . $newData ["A_FILEPATH"] . "', A_SYSTEM_USER='" . $newData ["A_SYSTEM_USER"] . "', A_EXPIRED_DATE='" . $newData ["A_EXPIRED_DATE"] . "', A_DOWNLOADKEY='" . $newData ["A_DOWNLOADKEY"] . "' where A_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
		}
		return 0;
	}
	
	/**
	 * Create object from Request.
	 */
	public function createEssence($newData) {
		$folderId = $newData ['folderid'];
		$kernel = ETSKernel::instance ();
		
		if (strcmp ( $folderId, "security_roles" ) == 0) {
			// Insert role
			$query = "INSERT INTO ANYSSH_ROLES (A_NAME, A_CODE) VALUES  ('" . $newData ["A_NAME"] . "','" . $newData ["A_CODE"] . "')";
			$kernel->getDB ()->executeQuery ( $query );
		} else if (strcmp ( $folderId, "security_users" ) == 0) {
			$password1 = $newData ["A_PASSWORD"];
			$password2 = $newData ["A_PASSWORD2"];
			if (strcmp ( $password1, $password2 ) != 0) {
				// Password not equals
				return "Password not equals.";
			} else {
				// Create users
				$query = "INSERT INTO ANYSSH_USERS (A_LOGIN, A_PASSWORD, A_EMAIL, A_FIO, A_ORGANIZATION, A_ENABLED, A_SSHKEY, A_EMAIL_VALID) VALUES ('" . $newData ["A_LOGIN"] . "','" . md5 ( $newData ["A_PASSWORD"] ) . "','" . $newData ["A_EMAIL"] . "','" . $newData ["A_FIO"] . "','" . $newData ["A_ORGANIZATION"] . "'," . $newData ["A_ENABLED"] . ",'" . $newData ["A_SSHKEY"] . "','" . $newData ["A_EMAIL_VALID"] . "')";
				$kernel->getDB ()->executeQuery ( $query );
			}
		} else if (strcmp ( $folderId, "security_userroles" ) == 0) {
			// Create users
			$query = "INSERT INTO ANYSSH_USERROLES (A_USERS_OUID, A_ROLES_OUID) VALUES (" . $newData ["user"] . "," . $newData ["role"] . ")";
			$kernel->getDB ()->executeQuery ( $query );
		} else if (strcmp ( $folderId, "anyssh_files" ) == 0) {
			// Create file
			self::generateFileForUser($newData ["A_USERS_OUID"]);
			$query = "INSERT INTO ANYSSH_FILES (A_USERS_OUID, A_CREATED_DATE, A_FILEPATH, A_SYSTEM_USER, A_EXPIRED_DATE, A_DOWNLOADKEY) VALUES (" . $newData ["A_USERS_OUID"] . ",'" . $newData ["A_CREATED_DATE"] .  "','" . $newData ["A_FILEPATH"] . "','" . $newData ["A_SYSTEM_USER"] . "','" . $newData ["A_EXPIRED_DATE"] . "','" . $newData ["A_DOWNLOADKEY"] . "')";
			$kernel->getDB ()->executeQuery ( $query );
		}
	}
	
	/**
	 * Remove object from Request.
	 */
	public function removeEssence($newData) {
		$folderId = $newData ['folderid'];
		$essenceId = $newData ['essenceId'];
		$kernel = ETSKernel::instance ();
		if (strcmp ( $folderId, "security_roles" ) == 0) {
			// Link to role
			$query = "delete from ANYSSH_USERROLES where A_ROLES_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
			// Delete role
			$query = "delete from ANYSSH_ROLES where A_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
		} else if (strcmp ( $folderId, "security_users" ) == 0) {
			// Link to role
			$query = "delete from ANYSSH_USERROLES where A_USERS_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
			// Linked files
			$query = "delete from ANYSSH_FILES where A_USERS_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
			// Delete role
			$query = "delete from ANYSSH_USERS where A_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
		} else if (strcmp ( $folderId, "security_userroles" ) == 0) {
			// Delete user role
			$query = "delete from ANYSSH_USERROLES where A_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
		} else if (strcmp ( $folderId, "anyssh_files" ) == 0) {
			// Delete files
			$query = "delete from ANYSSH_FILES where A_OUID=" . $essenceId;
			$kernel->getDB ()->executeQuery ( $query );
		}
		return 0;
	}
	
	/**
	 * Return all files for current logged user.
	 */
	public function getFilesByLoggedInUser() {
		$kernel = ETSKernel::instance ();
		$query = "SELECT * from ANYSSH_FILES where a_users_ouid=" . $_SESSION ['userId'] . " order by A_CREATED_DATE desc";
		$rs = $kernel->getDB ()->executeSelectQuery ( $query );
		return $rs;
	}
}

?>
