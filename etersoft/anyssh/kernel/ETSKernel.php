<?php
namespace etersoft\anyssh\kernel;

use etersoft\anyssh\db\ETSDBMySql;

/**
 * Main class.
 * @author 		Maxim Kuzmik
 * @copyright	(c) 2012 - Etersoft.
 * @license		GPL
 * @package		etersoft.anyssh.db
 * @link		http://www.etersoft.com
 * @since		1.0
 * @version		1.0
 */
class ETSKernel {
	
	/**
	 * Holds instance of ETSKernel (singleton implementation)
	 *
	 * @access private
	 * @var object
	 */
	private static $instance;
	
	/**
	 * ETSKernel initialized yet?
	 *
	 * @access private
	 * @var boolean
	 */
	private static $isInit = FALSE;
	
	/**
	 * Settings
	 *
	 * @access public
	 * @var array
	 */
	public static $settings = array ();
	
	/**
	 *
	 * @access private
	 * @var ETSDB
	 */
	private static $DB;
	
	/**
	 * Initialize singleton
	 *
	 * @access public
	 * @return object
	 */
	public static function instance() {
		if (! self::$instance) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	/**
	 * Non public constructor.
	 */
	private function __construct() {
	}
	
	/**
	 * Initiate the registry
	 *
	 * @access public
	 * @return mixed or void
	 */
	public function init() {
		if (self::$isInit === TRUE) {
			return FALSE;
		}
		self::$isInit = TRUE;
		
		/*
		 * Load config file
		 */
		if (file_exists ( ETS_ROOT_PATH . "config.php" )) {
			require ETS_ROOT_PATH . "config.php";
			if (is_array ( $ETS_CONFIG )) {
				foreach ( $ETS_CONFIG as $key => $val ) {
					ETSKernel::$settings [$key] = $val;
				}
			}
		}
		
		/*
		 * Init DB
		 */
		self::$DB = new ETSDBMySql ();
		self::$DB->connect ();
	}
	
	/**
	 * return DB implementation.
	 */
	public function getDB() {
		return self::$DB;
	}
	
	/**
	 * Return setting by it code.
	 * @param unknown_type $settingCode
	 */
	public function getSetting($settingCode) {
		return $this->$settings [$settingCode];
	}
	
	/**
	 * Return user id if user is logger, else - 0.
	 */
	public function isUserLogged() {
		if (isset ( $_SESSION ['userId'] )) {
			return $_SESSION ['userId'];
		} else {
			if (isset ( $_COOKIE ['etsuser_id'] )) {
				return $_COOKIE ['etsuser_id'];
			} else {
				return "0";
			}
		}
		return "0";
	}
	
	/**
	 * Get user by his ouid.
	 *
	 * @param $userId unknown_type       	
	 */
	public function getUserById($userId) {
		$query = "SELECT * from ANYSSH_USERS where A_OUID=" . $userId;
		
		$rs = self::$DB->executeSelectQuery ( $query );
		if (mysql_num_rows ( $rs ) == 1) {
			$row = mysql_fetch_assoc ( $rs );
			return $row;
		}
	}
	
	/**
	 * Get user by his login.
	 *
	 * @param $userId unknown_type       	
	 */
	public function getUserByLogin($userLogin) {
		$query = "SELECT * from ANYSSH_USERS where A_LOGIN='" . $userLogin . "'";
		
		$rs = self::$DB->executeSelectQuery ( $query );
		if (mysql_num_rows ( $rs ) == 1) {
			$row = mysql_fetch_assoc ( $rs );
			return $row;
		}
	}
	
	/**
	 * Get user by his login.
	 *
	 * @param $userId unknown_type       	
	 */
	public function isUserInRole($userId, $roleCode) {
		$query = "SELECT * from ANYSSH_USERROLES ur, ANYSSH_ROLES r where ur.A_OUID=" . $userId . " and r.A_CODE='" . $roleCode . "'";
		
		$rs = self::$DB->executeSelectQuery ( $query );
		if (mysql_num_rows ( $rs ) == 1) {
			return true;
		} else {
			return false;
		}
	}

}

?>