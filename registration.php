<?php
#################################################################################################
#
#  project                   : Etersoft - AnySSH WebSite
#  filename                  : index.php
#  version                   : 1.0
#  last modified by          : Kuzmik Maxim
#  e-mail                    : forn@etersoft.ru
#  purpose                   : Registerration page.
#  last modified             : 30.01.2012
#
#################################################################################################
?>

<?php
session_start ();
require_once 'init.php';

use etersoft\anyssh\kernel\ETSKernel;
use etersoft\anyssh\ETSActionListiner;

$CURRENTUSER = null;
$kernel = ETSKernel::instance ();
$kernel->init ();
$actionListiner = ETSActionListiner::instance ();
require 'includes/registration.php';
$kernel->getDB ()->disconnect ();
?>