<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : /controlpanel/includes/action.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : Proccess actions from control panel
// last modified : 31.01.2012
//
// ###############################################################################################
?>
<?php

require_once '../../init.php';

use etersoft\anyssh\kernel\ETSKernel;
use etersoft\anyssh\ETSActionListiner;

$kernel = ETSKernel::instance ();
$kernel->init ();
$actionListiner = ETSActionListiner::instance ();

if (isset ( $_REQUEST ['etsaction'] )) {
	if (strcmp ( $_REQUEST ['etsaction'], "userlogout" ) == 0) {
		$actionListiner->processAction (0);
	} else if (strcmp ( $_REQUEST ['etsaction'], "getfoldercontainerasxml" ) == 0) {
		$folderId = $_REQUEST ['folderid'];
		if (strcmp ( $folderId, "security_roles" ) == 0) {
			// List of roles
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlGrid_Roles ();
		} else if (strcmp ( $folderId, "security_users" ) == 0) {
			// List of users
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlGrid_Users ();
		} else if (strcmp ( $folderId, "anyssh_files" ) == 0) {
			// List of files
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlGrid_AnySSHFiles ();
		} else if (strcmp ( $folderId, "security_userroles" ) == 0) {
			// List of users permitions
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlGrid_UsersPermitions ();
		} 
	} else if (strcmp ( $_REQUEST ['etsaction'], "getessenceeditform" ) == 0) {
		$folderId = $_REQUEST ['folderid'];
		$essenceId = $_REQUEST ['essenceId'];
		header ( "Content-type: text/xml" );
		echo $actionListiner->generateXmlEditForm ( $folderId, $essenceId );
	} else if (strcmp ( $_REQUEST ['etsaction'], "updateEssence" ) == 0) {
		echo $actionListiner->updateEssence ( $_REQUEST );
	} else if (strcmp ( $_REQUEST ['etsaction'], "getessencecreateform" ) == 0) {
		$folderId = $_REQUEST ['folderid'];
		if (strcmp ( $folderId, "security_roles" ) == 0) {
			// Create role
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlCreateForm_Roles ( $folderId );
		} else if (strcmp ( $folderId, "security_users" ) == 0) {
			// Create user
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlCreateForm_Users ( $folderId );
		}  else if (strcmp ( $folderId, "security_userroles" ) == 0) {
			// Create user
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlCreateForm_UsersPermitions ( $folderId );
		} else if (strcmp ( $folderId, "anyssh_files" ) == 0) {
			// Create user
			header ( "Content-type: text/xml" );
			echo $actionListiner->generateXmlCreateForm_AnySSHFiles ( $folderId );
		} 
	} else if (strcmp ( $_REQUEST ['etsaction'], "createEssence" ) == 0) {
		echo $actionListiner->createEssence ( $_REQUEST );
	} else if (strcmp ( $_REQUEST ['etsaction'], "removeEssence" ) == 0) {
		echo $actionListiner->removeEssence ( $_REQUEST );
	}
}
?>