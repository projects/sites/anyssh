<?php
#################################################################################################
#
#  project                   : Etersoft - AnySSH WebSite
#  filename                  : /controlpanel/includes/work.php
#  version                   : 1.0
#  last modified by          : Kuzmik Maxim
#  e-mail                    : forn@etersoft.ru
#  purpose                   : Main work content for control panel
#  last modified             : 25.01.2012
#
#################################################################################################
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../anyssh_admin.css">
<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlx.css">
<script src="../dhtmlx/dhtmlx.js"></script>
<script src="../js/admin.js"></script>
<title>Etersoft - AnySSH - Панель управления</title>
</head>
<script>
userLogin = "<?php echo $CURRENTUSER['A_LOGIN']?>";
userId = "<?php echo $_SESSION['userId'];?>";
</script>
<body>
</body>
</html>