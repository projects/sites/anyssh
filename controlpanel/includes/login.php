<?php
#################################################################################################
#
#  project                   : Etersoft - AnySSH WebSite
#  filename                  : /controlpanel/includes/login.php
#  version                   : 1.0
#  last modified by          : Kuzmik Maxim
#  e-mail                    : forn@etersoft.ru
#  purpose                   : Login content for control panel
#  last modified             : 26.01.2012
#
#################################################################################################
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../anyssh_admin.css">
<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlx.css">
<script src="../dhtmlx/dhtmlx.js"></script>
<title>Etersoft - AnySSH - Панель управления</title>
</head>
<body onLoad="login();">

<div id="loginFormDiv"></div>

<script>
function login(){
	var loginForm = new dhtmlXForm("loginFormDiv");
	loginForm.loadStruct("../xml/form_login_admin.xml?r="+Math.random());
	loginForm.attachEvent("onButtonClick", loginForm_onButtonClick);

	function loginForm_onButtonClick(btnName){
		if (btnName=="btnSubmit"){
			loginForm.send("cp.php","post",function(response){
				if ((response.xmlDoc.responseText == "0") || (response.xmlDoc.responseText == 0)){
					window.location.reload(true);
				} else {
					alert("Error:"+response.xmlDoc.responseText);
				}
			});
		}
	}
}
</script>

</body>
</html>