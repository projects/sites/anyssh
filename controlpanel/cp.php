<?php
#################################################################################################
#
#  project                   : Etersoft - AnySSH WebSite
#  filename                  : /controlpanel/cp.php
#  version                   : 1.0
#  last modified by          : Kuzmik Maxim
#  e-mail                    : forn@etersoft.ru
#  purpose                   : Main file for control panel.
#  last modified             : 25.01.2012
#
#################################################################################################
?>

<?php
session_start ();
require_once '../init.php';

use etersoft\anyssh\kernel\ETSKernel;
use etersoft\anyssh\ETSActionListiner;

$kernel = ETSKernel::instance ();
$kernel->init ();
$actionListiner = ETSActionListiner::instance ();

$CURRENTUSER = null;
$result = $actionListiner->isUserLogged ();
if (isset ( $_REQUEST ['etsaction'] )) {
	echo $actionListiner->processAction (0);
} else if (strcmp ( $result, "0" ) != 0) {
	$CURRENTUSER = $kernel->getUserById ( $result );
	require 'includes/work.php';
} else {
	require 'includes/login.php';
}
$kernel->getDB ()->disconnect ();
?>