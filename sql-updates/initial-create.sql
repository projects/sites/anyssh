--DDL
CREATE TABLE ANYSSH_USERS (
  A_OUID         int(22) NOT NULL AUTO_INCREMENT comment 'Identity', 
  A_LOGIN        varchar(30) NOT NULL UNIQUE comment 'Login', 
  A_PASSWORD     varchar(50) NOT NULL comment 'Password', 
  A_EMAIL        varchar(100) NOT NULL UNIQUE comment 'E-mail', 
  A_FIO          varchar(200) NOT NULL comment 'User name', 
  A_ENABLED      int(1) NOT NULL comment 'Enabled of disabled.', 
  A_ORGANIZATION varchar(200) comment 'Organization name', 
  A_SSHKEY       longtext comment 'SSH-Key', 
  A_EMAIL_VALID  varchar(100) comment 'Key for validation by E-mail', 
  CONSTRAINT PK_ANYSSH_USERS 
    PRIMARY KEY (A_OUID)) comment='Users of the system';
CREATE TABLE ANYSSH_ROLES (
  A_OUID int(22) NOT NULL AUTO_INCREMENT comment 'Identity', 
  A_NAME varchar(100) NOT NULL comment 'Name of role', 
  A_CODE varchar(30) NOT NULL UNIQUE comment 'Code of role', 
  CONSTRAINT PK_ANYSSH_ROLES 
    PRIMARY KEY (A_OUID)) comment='Roles of the system';
CREATE TABLE ANYSSH_USERROLES (
  A_OUID       int(22) NOT NULL AUTO_INCREMENT comment 'Identity', 
  A_USERS_OUID int(22) NOT NULL comment 'User', 
  A_ROLES_OUID int(22) NOT NULL comment 'Role', 
  CONSTRAINT PK_ANYSSH_USERROLES 
    PRIMARY KEY (A_OUID)) comment='Links between the user and his roles';
CREATE TABLE ANYSSH_SETTINGS (
  A_OUID  int(22) NOT NULL AUTO_INCREMENT comment 'Identity', 
  A_NAME  varchar(100) NOT NULL comment 'Name of setting', 
  A_CODE  varchar(30) NOT NULL UNIQUE comment 'Code of setting', 
  A_VALUE varchar(500) NOT NULL comment 'Value of setting', 
  CONSTRAINT PK_ANYSSH_SETTINGS 
    PRIMARY KEY (A_OUID)) comment='System Settings';
CREATE TABLE ANYSSH_FILES (
  A_OUID         int(22) NOT NULL AUTO_INCREMENT comment 'Identity', 
  A_USERS_OUID   int(22) NOT NULL comment 'Owner of file', 
  A_CREATED_DATE datetime NOT NULL comment 'Date of the creation', 
  A_FILEPATH     varchar(1000) NOT NULL comment 'Path to the location of the file and file name. (Root directory is located in the settings)', 
  A_SYSTEM_USER  varchar(100) NOT NULL comment 'User name if the system', 
  A_EXPIRED_DATE datetime NOT NULL comment 'The expiration date of the key', 
  A_DOWNLOADKEY  varchar(100) NOT NULL comment 'Key for download', 
  CONSTRAINT PK_ANYSSH_FILES 
    PRIMARY KEY (A_OUID));
ALTER TABLE ANYSSH_USERROLES ADD INDEX RelUserToRole (A_USERS_OUID), ADD CONSTRAINT RelUserToRole FOREIGN KEY (A_USERS_OUID) REFERENCES ANYSSH_USERS (A_OUID);
ALTER TABLE ANYSSH_USERROLES ADD INDEX RelRoleToUser (A_ROLES_OUID), ADD CONSTRAINT RelRoleToUser FOREIGN KEY (A_ROLES_OUID) REFERENCES ANYSSH_ROLES (A_OUID);
ALTER TABLE ANYSSH_FILES ADD INDEX RelUSerToFile (A_USERS_OUID), ADD CONSTRAINT RelUSerToFile FOREIGN KEY (A_USERS_OUID) REFERENCES ANYSSH_USERS (A_OUID);
CREATE UNIQUE INDEX UK_USERROLES ON ANYSSH_USERROLES (A_USERS_OUID, A_ROLES_OUID);
CREATE INDEX IDX_USER_FOR_ROLES ON ANYSSH_USERROLES (A_USERS_OUID);