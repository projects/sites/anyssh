<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : index.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : Download file.
// last modified : 30.01.2012
//
// ###############################################################################################

session_start ();

require_once 'init.php';

use etersoft\anyssh\kernel\ETSKernel;
use etersoft\anyssh\ETSActionListiner;

if (isset ( $_REQUEST ["key"] )) {
	$fileKey = $_REQUEST ["key"];
	$kernel = ETSKernel::instance ();
	$kernel->init ();
	$actionListiner = ETSActionListiner::instance ();
	
	$file = $actionListiner->getAnySSHFileByKey ( $fileKey );
	
	$path = $_SERVER ['DOCUMENT_ROOT'] . "/"; // change the path to fit your
	                                       // websites document structure
	$fullPath = $file ['A_FILEPATH'];
	
	if ($fd = fopen ( $fullPath, "r" )) {
		$fsize = filesize ( $fullPath );
		$path_parts = pathinfo ( $fullPath );
		$ext = strtolower ( $path_parts ["extension"] );
		switch ($ext) {
			default :
				header ( "Content-type: application/octet-stream" );
				header ( "Content-Disposition: filename=\"" . $path_parts ["basename"] . "\"" );
		}
		header ( "Content-length: $fsize" );
		header ( "Cache-control: private" ); // use this to open files directly
		while ( ! feof ( $fd ) ) {
			$buffer = fread ( $fd, 2048 );
			echo $buffer;
		}
	}
	fclose ( $fd );
	exit ();
}

?>