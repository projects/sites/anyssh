<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : /includes/registration.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : Registration page.
// last modified : 30.01.2012
//
// ###############################################################################################
?>

<?php 
if(isset($_GET['lang'])) {
	$_SESSION['lang'] = $_GET['lang'];
}
if(!isset($_SESSION['lang'])) {
	$_SESSION['lang'] = 'en'; // default value
}
include 'etersoft/anyssh/localization/'.$_SESSION['lang'] . '.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?= $localization['Site.Title'];?> - <?= $localization['Site.Title.Registration'];?></title>
<link rel="stylesheet" type="text/css" href="anyssh.css">
<script type="text/javascript" src="js/jquery-1.7.1.js" /></script>
</head>

<style type="text/css">
html,body {
	height: 100%;
}
</style>

<script type="text/javascript">
$(document).ready(function() {

	$("#btnRegistration").click(function() {
 		var action = $("#formRegistration").attr('action');
		var form_data = {
			user: $("#user").val(),
			password: $("#password").val(),
			password2: $("#password2").val(),
			email: $("#email").val(),
			fullName: $("#fullName").val(),
			companyName: $("#companyName").val(),
			sshKey: $("#sshKey").val(),
			etsaction:  $("#etsaction").val(),
			is_ajax: 1
		};
 
		$.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response){
				if ((response == '0') || (response == 0)){
					$("#formRegistration").slideUp('slow', function() {
						$("#message").html("<p class='success'><?= $localization['Messages.Register.Success'];?></p>");
					});
				}else{
					$("#message").html("<p class='error'>"+response+"</p>");
				}
			}
		});
 
		return false;
	});
 
});
</script>

<body style="background: #cfddea repeat-x;">
	<table width="100%" height="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="background: url(images/logo.png) center top no-repeat"
				align="center">
				<table class="panel" cellpadding="0" cellspacing="0"
					style="width: 300px">
					<tr>
						<td class="tl"></td>
						<td class="tm"></td>
						<td class="tr"></td>
					</tr>
					<tr>
						<td class="lm"></td>
						<td class="c">
							<table class="ftable">
								<tr>
									<td colspan="2" align="center"><div id="message"></div></td>
								</tr>
							</table>
							<form action="includes/action.php" method="post"
								id="formRegistration">
								<table class="ftable">
									<tr>
										<td colspan="2" align="center"><b><?= $localization['Site.Title.Registration'];?></b></td>
									</tr>

									<tr>
										<th><?= $localization['Essence.User.Login'];?>:</th>
										<td><input type="text" name="user" id="user"
											style="width: 300px" /></td>
									</tr>
									<tr>
										<th><?= $localization['Essence.User.Password'];?>:</th>
										<td><input type="password" name="password" id="password"
											style="width: 300px" /></td>
									</tr>
									<tr>
										<th><?= $localization['Essence.User.ConfirmPassword'];?>:</th>
										<td><input type="password" name="password2" id="password2"
											style="width: 300px" /></td>
									</tr>
									<tr>
										<th><?= $localization['Essence.User.Email'];?>:</th>
										<td><input type="text" name="email" id="email"
											style="width: 300px" /></td>
									</tr>
									<tr>
										<th><?= $localization['Essence.User.FullName'];?>:</th>
										<td><input type="text" name="fullName" id="fullName"
											style="width: 300px" /></td>
									</tr>
									<tr>
										<th><?= $localization['Essence.User.CompanyName'];?>:</th>
										<td><input type="text" name="companyName" id="companyName"
											style="width: 300px" /></td>
									</tr>
									<tr>
										<th><?= $localization['Essence.User.SSHKey'];?>:</th>
										<td><textarea rows="5" name="sshKey" id="sshKey"
												style="width: 300px"></textarea></td>
									</tr>
									<tr>
										<td colspan="2" align="center"><input type="hidden"
											id="etsaction" value="userRegistration" /> <input
											type="submit" id="btnRegistration" value="<?= $localization['Action.Register'];?>" /></td>
									</tr>
								</table>
							</form>
						</td>
						<td class="rm"></td>
					</tr>
					<tr>
						<td class="bl"></td>
						<td class="bm"></td>
						<td class="br"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>