<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : /includes/action.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : Proccess all user action.
// last modified : 30.01.2012
//
// ###############################################################################################
?>

<?php
session_start ();
require_once '../init.php';

use etersoft\anyssh\kernel\ETSKernel;
use etersoft\anyssh\ETSActionListiner;

$kernel = ETSKernel::instance ();
$kernel->init ();
$actionListiner = ETSActionListiner::instance ();

$is_ajax = 0;
if (isset ( $_REQUEST ['is_ajax'])){
	$is_ajax = $_REQUEST ['is_ajax'];
}

if (isset ( $_REQUEST ['etsaction'] )) {
	if (isset ( $is_ajax ) && $is_ajax) {
		echo $actionListiner->processAction ($is_ajax);
	} else {
		$actionListiner->processAction (0);
	}
}

?>