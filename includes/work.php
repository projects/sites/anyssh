<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : /includes/work.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : Main page for user.
// last modified : 01.02.2012
//
// ###############################################################################################
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?= $localization['Site.Title'];?></title>
<link rel="stylesheet" type="text/css" href="anyssh.css">
<script type="text/javascript" src="js/jquery-1.7.1.js" /></script>
</head>

<style type="text/css">
html,body {
	height: 100%;
}
</style>

<script type="text/javascript">
$(document).ready(function() {

	$("#btnLogout").click(function() {
 		var action = $("#formLogout").attr('action');
		var form_data = {
			etsaction:  $("#etsaction").val(),
			is_ajax: 1
		};
 
		$.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response){
				if ((response == '0') || (response == 0)){
					window.location.href = "index.php";
				}
			}
		});
 
		return false;
	});

	$("#btnRefreshDownloadKey").click(function() {
 		var action = $("#formRefreshDownloadKey").attr('action');
		var form_data = {
			etsaction:  $("#etsaction2").val(),
			fileId:  $("#fileId").val(),
			is_ajax: 1
		};
 
		$.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response){
				window.location.href = "index.php";
			}
		});
 
		return false;
	});

	$("#btnCreateNewFile").click(function() {
 		var action = $("#formCreateNewFile").attr('action');
		var form_data = {
			etsaction:  $("#etsaction3").val(),
			is_ajax: 1
		};
 
		$.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response){
				window.location.href = "index.php";
			}
		});
 
		return false;
	});
	
	
 
});
</script>

<body
	style="background: #cfddea url(images/view_head_background.png) repeat-x;">

	<table id="global" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 0 10px 0 10px"><span
				style="font-weight: bold; font-size: larger; cursor: pointer"
				onclick="document.location = ''"><?= $localization['Site.Title'];?></span></td>
			<td align="center"><span>Your login is: <? echo $CURRENTUSER['A_LOGIN'];?></span></td>
			<td align="right"><span style="font-size: xx-small;"><?= $localization['Site.Version'];?>:
					1.0</span></td>
			<td class="snap">
				<form action="includes/action.php" method="post" id="formLogout">
					<input type="hidden" id="etsaction" value="userlogout" /> <input
						type="submit" id="btnLogout"
						value="<?= $localization['Site.Logout'];?>" />
				</form>
			</td>
		</tr>
	</table>

	<div class="content">
		<table class="tbl" cellpadding="0" cellspacing="0" align="center"
			border="1">

			<thead>
				<tr>
					<th align="center" colspan="4"><?= $localization['Site.Files.List'];?></th>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<form action="includes/action.php" method="post" id="formCreateNewFile">
							<input type="hidden" id="etsaction3" value="createNewFile" /> <input
								type="submit" id="btnCreateNewFile"
								value="<?= $localization['Action.CreateFile'];?>" />
						</form>
					</td>
				</tr>
				<tr>
					<th><?= $localization['Essence.Files.DateofCreate'];?></th>
					<th><?= $localization['Essence.Files.ExpiredDate'];?></th>
					<th><?= $localization['Action.Download'];?></th>
					<th><?= $localization['Action.UpdateDownloadLink'];?></th>
				</tr>
				<?php
				$files = $actionListiner->getFilesByLoggedInUser ();
				if (isset ( $files )) {
					for($count = 1; $file = mysql_fetch_assoc ( $files ); ++ $count) {
						?>
					<tr>
					<td><?php echo $file["A_CREATED_DATE"];?></td>
					<td><?php echo $file["A_EXPIRED_DATE"];?></td>
					<td align="center"><a
						href="download.php?key=<?php echo $file["A_DOWNLOADKEY"];?>"><?= $localization['Action.Download'];?></a></td>
					<td align="center">
						<div id="message"></div>
						<form action="includes/action.php" method="post"
							id="formRefreshDownloadKey">
							<input type="hidden" id="etsaction2" value="refreshDownloadKey" />
							<input type="hidden" id="fileId"
								value="<?php echo $file["A_OUID"];?>" /> <input type="submit"
								id="btnRefreshDownloadKey"
								value="<?= $localization['Action.Refresh'];?>" />
						</form>
					</td>
				</tr>
				<?php
					}
				}
				?>
			</thead>
		</table>
	</div>

</body>
</html>