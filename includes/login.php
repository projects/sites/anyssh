<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : /includes/login.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : Login to the system.
// last modified : 31.01.2012
//
// ###############################################################################################
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?= $localization['Site.Title'];?> - <?= $localization['Site.Title.Login'];?></title>
<link rel="stylesheet" type="text/css" href="anyssh.css">
<script type="text/javascript" src="js/jquery-1.7.1.js" /></script>
</head>

<style type="text/css">
html,body {
	height: 100%;
}
</style>

<script type="text/javascript">
$(document).ready(function() {

	$("#btnLogin").click(function() {
 		var action = $("#formLogin").attr('action');
		var form_data = {
			user: $("#user").val(),
			password: $("#password").val(),
			etsaction:  $("#etsaction").val(),
			is_ajax: 1
		};
 
		$.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response){
				if ((response == '0') || (response == 0)){
					window.location.href = "index.php";
				}else{
					$("#message").html("<p class='error'>"+response+"</p>");
				}
			}
		});
 
		return false;
	});
 
});
</script>

<body style="background: #cfddea repeat-x;">
	<table width="100%" height="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="background: url(images/logo.png) center top no-repeat"
				align="center">
				<table class="panel" cellpadding="0" cellspacing="0"
					style="width: 300px">
					<tr>
						<td class="tl"></td>
						<td class="tm"></td>
						<td class="tr"></td>
					</tr>
					<tr>
						<td class="lm"></td>
						<td class="c">
							<form action="includes/action.php" method="post" id="formLogin">
								<table class="ftable">
									<tr>
										<td colspan="2" align="center"><div id="message"></div></td>
									</tr>
									<tr>
										<th><?= $localization['Site.Login.Login'];?>:</th>
										<td><input type="text" name="user" id="user" /></td>
									</tr>
									<tr>
										<th><?= $localization['Site.Login.Password'];?>:</th>
										<td><input type="password" name="password" id="password" /></td>
									</tr>
									<tr>
										<td colspan="2" align="center"><input type="hidden"
											id="etsaction" value="userlogin" /> <input type="submit"
											id="btnLogin"
											value="<?= $localization['Site.Login.ProccesLogin'];?>" /></td>
									</tr>
									<tr>
										<td colspan="2" align="center"><a href="registration.php"><?= $localization['Site.Login.Registration'];?></a></td>
									</tr>
								</table>
							</form>
						</td>
						<td class="rm"></td>
					</tr>
					<tr>
						<td class="bl"></td>
						<td class="bm"></td>
						<td class="br"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>