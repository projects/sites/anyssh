<?php
// ###############################################################################################
//
// project : Etersoft - AnySSH WebSite
// filename : /includes/languageselect.php
// version : 1.0
// last modified by : Kuzmik Maxim
// e-mail : forn@etersoft.ru
// purpose : UI for language select.
// last modified : 06.02.2012
//
// ###############################################################################################
?>
&nbsp;<a href="?lang=ru-ru">ru</a>&nbsp;<a href="?lang=en-en">en</a>&nbsp;