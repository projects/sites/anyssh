var layoutBody;
var layoutTree;
var layoutMain;
var mainToolBar;
var mainStatusBar;
var foldersTree;
var essenseListGrid;
var userLogin;
var userId;
var selectedFolder = -1;
var currentWindow;
var windowId = "winEssenceOperation";
var addEssenceForm;
var editEssenceForm;

dhtmlxEvent(window, "load", function() {
	initLayout();
});

dhtmlxError.catchError("LoadXML", function(errorType, error, data) {
	errorMessage = "Error loading data.";
	errorMessage += "\n " + "Error tpye:" + errorType;
	errorMessage += "\n " + "Error code:" + error;
	errorMessage += "\n " + "Response from server:" + data[0].responseText;
	alert(errorMessage);
});

// Main Layout inicialization
function initLayout() {
	layoutBody = new dhtmlXLayoutObject(document.body, "2U");

	layoutBody.dhxWins.setImagePath("../dhtmlx/imgs/");

	layoutTree = layoutBody.items[0];

	layoutTree.setText("Menu");
	layoutTree.setWidth(200);
	foldersTree = layoutTree.attachTree();
	foldersTree.setImagePath("../dhtmlx/imgs/anyssh/");

	layoutMain = layoutBody.items[1];
	layoutMain.setText("Data");

	mainToolBar = layoutBody.attachToolbar();
	initToolBar();

	mainStatusBar = layoutBody.attachStatusBar();
	setStatus("Ready");

	essenseListGrid = layoutMain.attachGrid();
	initEssenseListGrid();

	// Init Tree
	initTree();
}

// Инициализация таблицы с данными
function initEssenseListGrid() {
	essenseListGrid.setImagePath("../dhtmlx/imgs/");
	essenseListGrid.setEditable(false);
	essenseListGrid.setSkin("dhx_skyblue");
	essenseListGrid.attachEvent("onRowDblClicked",
			essenseListGrid_onRowDblClicked);
}

function essenseListGrid_onRowDblClicked() {
	displayEditEssenceWindow();
}

// Инициализация тулбара
function initToolBar() {
	mainToolBar.setIconsPath("../dhtmlx/imgs/anyssh/");
	mainToolBar.attachEvent("onClick", mainToolBar_onClick);

	mainToolBar.addText("txtLogin", 1, "Your login: " + userLogin);
	mainToolBar.addButton("btnLogout", 3, "", "exit.gif", "exit.gif");
	mainToolBar.setItemToolTip("btnLogout", "Logout");
	// Add button to the toolbar
	mainToolBar.addSeparator("sepLogin", 2);
	// Create essence
	mainToolBar.addButton("btnAddEssence", 3, "", "add.gif", "add_dis.gif");
	mainToolBar.setItemToolTip("btnAddEssence", "Create essence");
	mainToolBar.disableItem("btnAddEssence");
	// Edit essence
	mainToolBar.addButton("btnEditEssence", 4, "", "edit.gif", "edit_dis.gif");
	mainToolBar.setItemToolTip("btnEditEssence", "Edir essence");
	mainToolBar.disableItem("btnEditEssence");
	// Remove essence
	mainToolBar.addButton("btnRemoveEssence", 5, "", "delete.gif",
			"delete_dis.gif");
	mainToolBar.setItemToolTip("btnRemoveEssence", "Delete essence");
	mainToolBar.disableItem("btnRemoveEssence");
	// Refresh essence list
	mainToolBar.addSeparator("sepRefresh", 6);
	mainToolBar.addButton("btnRefreshGrid", 7, "", "refresh.gif",
			"refresh_dis.gif");
	mainToolBar.setItemToolTip("btnRefreshGrid", "Refresh grid");
	mainToolBar.disableItem("btnRefreshGrid");
}

function setStatus(statusTest) {
	mainStatusBar.setText(statusTest);
}

// Tree Initcialization
function initTree() {
	refreshFolderTree();
	foldersTree.attachEvent("onSelect", function(id, idPrev) {
		disableToolBarButtons();
		selectedFolder = id;
		if (selectedFolder != 'security') {
			setStatus("Loading data...");
			refreshEssenceGrid();
		} else {

		}
	});
}

function disableToolBarButtons() {
	mainToolBar.disableItem("btnAddEssence");
	mainToolBar.disableItem("btnEditEssence");
	mainToolBar.disableItem("btnRemoveEssence");
	mainToolBar.disableItem("btnRefreshGrid");
}

function enableToolBarButtons() {
	mainToolBar.enableItem("btnAddEssence");
	mainToolBar.enableItem("btnEditEssence");
	mainToolBar.enableItem("btnRemoveEssence");
	mainToolBar.enableItem("btnRefreshGrid");
}

function displayEditEssenceWindow() {
	var selectedRow = essenseListGrid.getSelectedRowId();
	if (selectedRow == null) {
		alert("You must select essence for edit!!!");
	} else {
		setStatus("Loading edit form...");
		currentWindow = layoutBody.dhxWins.createWindow(windowId, 100, 100,
				400, 600);
		currentWindow.center();
		currentWindow.setModal(true);
		currentWindow.setText("Essence edit");
		editEssenceForm = currentWindow.attachForm();

		// Request form data
		selectedEssenceId = essenseListGrid.cellById(selectedRow, 0).getValue();
		var params = "etsaction=getessenceeditform&folderid=" + selectedFolder
				+ "&essenceId=" + selectedEssenceId;
		dhtmlxAjax.post("includes/action.php?r=" + Math.random(), params,
				function(loader) {
					var formData = loader.xmlDoc.responseText;
					// alert(formData);
					editEssenceForm.loadStructString(formData, function() {
						setStatus("Ready.");
					});
				});
		editEssenceForm.attachEvent("onButtonClick",
				editEssenceForm_onButtonClick);
		editEssenceForm.attachEvent("onAfterValidate", function(id, result) {
			if (result == true) {
				setStatus("Sending data to the server...");
			}
		});
	}
}

function mainToolBar_onClick(buttonId) {
	var selectedRow = essenseListGrid.getSelectedRowId();
	if (buttonId == "btnLogout") {
		var params = "etsaction=userlogout";
		dhtmlxAjax.post("cp.php?r=" + Math.random(), params, function(loader) {
			window.location.href = "cp.php";
		});
	} else if (buttonId == "btnAddEssence") {
		setStatus("Loading form data...");
		currentWindow = layoutBody.dhxWins.createWindow(windowId, 100, 100,
				400, 600);
		currentWindow.center();
		currentWindow.setModal(true);
		currentWindow.setText("Create new essence");
		addEssenceForm = currentWindow.attachForm();

		// Request form data
		var params = "etsaction=getessencecreateform&folderid="
				+ selectedFolder;
		dhtmlxAjax.post("includes/action.php?r=" + Math.random(), params,
				function(loader) {
					var formData = loader.xmlDoc.responseText;
					addEssenceForm.loadStructString(formData, function() {
						setStatus("Ready.");
					});
				});
		addEssenceForm.attachEvent("onButtonClick",
				addEssenceForm_onButtonClick);
		addEssenceForm.attachEvent("onAfterValidate", function(id, result) {
			if (result == true) {
				setStatus("Sending data to the server...");
			}
		});
	} else if (buttonId == "btnEditEssence") {
		displayEditEssenceWindow();
	} else if (buttonId == "btnRemoveEssence") {
		if (selectedRow == null) {
			alert("You must select eesene for delete!!!");
		} else {
			selectedEssenceId = essenseListGrid.cellById(selectedRow, 0)
					.getValue();
			if (confirm("Are you sure for delete essence with id:"
					+ selectedEssenceId + "?")) {
				var params = "etsaction=removeEssence&folderid="
						+ selectedFolder + "&essenceId=" + selectedEssenceId;
				setStatus("Sending data to the server...");
				dhtmlxAjax.post(
						"includes/action.php?r=" + Math.random(),
						params, function(response) {
							if ((response.xmlDoc.responseText == "0") || (response.xmlDoc.responseText == 0)) {
								setStatus("Ready.");
								refreshEssenceGrid();
							} else {
								setStatus("Ready.");
								alert("Error while save data on server:"
										+ response.xmlDoc.responseText);
							}
						});
			}
		}
	} else if (buttonId == "btnRefreshGrid") {
		refreshEssenceGrid();
	}
}

function refreshEssenceGrid() {
	setStatus("Loading data from server...");
	var params = "etsaction=getfoldercontainerasxml&folderid=" + selectedFolder;
	dhtmlxAjax.post("includes/action.php?r=" + Math.random(), params, function(
			loader) {
		var data = loader.xmlDoc.responseText;
		// alert(data);
		if (data != "") {
			essenseListGrid.parse(data, function() {
				enableToolBarButtons();
				setStatus("Ready");
			});
		} else {
			setStatus("Emptry folder.");
			mainToolBar.enableItem("btnAddFolder");
		}
	});
}

function addEssenceForm_onButtonClick(btnName) {
	if (btnName == "btnAddSubmit") {
		addEssenceForm.send("includes/action.php", "post", function(response) {
			if ((response.xmlDoc.responseText == "0") || (response.xmlDoc.responseText == 0)) {
				setStatus("Ready.");
				refreshEssenceGrid();
				currentWindow.close();
			} else {
				setStatus("Ready.");
				alert("Error while save data on server:"
						+ response.xmlDoc.responseText);
			}
		});
	}
}

function refreshFolderTree() {
	foldersTree.deleteChildItems(0);
	setStatus("Loading menu...");
	dhtmlxAjax.get("../xml/controlpanel/xmlmenu.xml?r=" + Math.random(),
			function(loader) {
				var data = loader.xmlDoc.responseText;
				// alert(data);
				foldersTree.loadXMLString(data);
				setStatus("Ready");
			});
}

function editEssenceForm_onButtonClick(btnName) {
	if (btnName == "btnEditSubmit") {
		editEssenceForm.send("includes/action.php", "post", function(response) {
			if ((response.xmlDoc.responseText == "0") || (response.xmlDoc.responseText == 0)) {
				setStatus("Ready.");
				refreshEssenceGrid();
				currentWindow.close();
			} else {
				setStatus("Ready.");
				alert("Error while save data on server:"
						+ response.xmlDoc.responseText);
			}
		});
	}
}
